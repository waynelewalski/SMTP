﻿namespace Scribe.Connector.SMTP
{
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "{117BFDD7-8EF4-48AF-980E-9BD8DE56D01E}";
        public const string ConnectorVersion = "1.0";
        public const string Description = "A Scribe Labs Connector For Sending Email Via An SMTP Server. Please see http://www.scribesoft.com/scribelabs for important details.";
        public const string Name = "SMTP Connector";
        public const bool SupportsCloud = true;
    }
}