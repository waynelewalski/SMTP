﻿namespace Scribe.Connector.SMTP
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Scribe.Core.ConnectorApi;
    using Scribe.Core.ConnectorApi.Metadata;

    public class MetadataProvider : IMetadataProvider
    {

        private IEnumerable<IActionDefinition> actionDefinitions;

        private IEnumerable<IObjectDefinition> objectDefinitions;
        //private Uri serviceUri;

        //private Uri MetaDataUri;

        public MetadataProvider()
        {
            // TODO: Complete member initialization


        }


        private IEnumerable<IActionDefinition> ActionDefinitions { get { return this.actionDefinitions ?? (this.actionDefinitions = this.GetActionDefinitions()); } }

        private IEnumerable<IObjectDefinition> ObjectDefinitions { get { return this.objectDefinitions ?? (this.objectDefinitions = this.GetObjectDefinitions()); } }

        private IEnumerable<IActionDefinition> GetActionDefinitions()
        {
            return new List<IActionDefinition>
                   {
                       new ActionDefinition
                       {
                           Description = "Send Email",
                           FullName = "Send Email",
                           KnownActionType = KnownActions.Create,
                           Name = "Send Email",
                           SupportsBulk = false,
                           SupportsConstraints = false,
                           SupportsInput = true,
                           SupportsLookupConditions = false,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = false,
                           SupportsSequences = false
                       },
                   };
        }

        private IEnumerable<IObjectDefinition> GetObjectDefinitions()
        {
            return new List<IObjectDefinition>
                   {
                       new ObjectDefinition
                       {
                           FullName = "Email",
                           Description = "Send Notification to an email address",
                           Hidden = false,
                           Name = "Email",
                           SupportedActionFullNames = new List<string>{"Send Email"},
                           PropertyDefinitions = new List<IPropertyDefinition>
                                                 {
                                                     new PropertyDefinition
                                                     {
                                                         Name = "To",
                                                         FullName = "To",
                                                         Description = "Email address of recipient",
                                                         IsPrimaryKey = false,
                                                         Nullable = false,
                                                         PresentationType = "string",
                                                         PropertyType = typeof(string).Name,
                                                         UsedInActionInput = true,
                                                         RequiredInActionInput = true,
                                                         UsedInActionOutput = false,
                                                         UsedInQuerySelect = false,
                                                     },
                                                     //new PropertyDefinition
                                                     //{
                                                     //    Name = "CC",
                                                     //    FullName = "CC",
                                                     //    Description = "Email address of recipient",
                                                     //    IsPrimaryKey = false,
                                                     //    Nullable = false,
                                                     //    PresentationType = "string",
                                                     //    PropertyType = typeof(string).Name,
                                                     //    UsedInActionInput = true,
                                                     //    RequiredInActionInput = true,
                                                     //    UsedInActionOutput = false,
                                                     //    UsedInQuerySelect = false,
                                                     //},
                                                     new PropertyDefinition
                                                     {
                                                        Name = "Subject",
                                                        FullName = "Subject",
                                                        Description = "Email Subject",
                                                        IsPrimaryKey = false,
                                                        Nullable = false,
                                                        UsedInQuerySelect = false,
                                                        UsedInActionInput = true,
                                                        RequiredInActionInput = true,
                                                        PropertyType = typeof(string).Name
                                                     },
                                                     new PropertyDefinition
                                                     {
                                                        Name = "Body",
                                                        FullName = "Body",
                                                        Description = "Email Body",
                                                        IsPrimaryKey = false,
                                                        Nullable = false,
                                                        UsedInQuerySelect = false,
                                                        UsedInActionInput = true,
                                                        RequiredInActionInput = true,
                                                        PropertyType = typeof(string).Name
                                                     },
                                                     //new PropertyDefinition
                                                     //{
                                                     //    Name = "Attachment",
                                                     //    FullName = "Attachment",
                                                     //    Description = "Email Attachment",
                                                     //    IsPrimaryKey = false,
                                                     //    Nullable = true,
                                                     //    PropertyType = typeof(double).Name,
                                                     //    UsedInActionInput = false,
                                                     //    RequiredInActionInput = false,
                                                     //    UsedInActionOutput = true,
                                                     //    UsedInQuerySelect = false,
                                                     //},
                                                     new PropertyDefinition
                                                     {
                                                         Name = "SendResult",
                                                         FullName = "SendResult",
                                                         Description = "Email Send Results",
                                                         IsPrimaryKey = false,
                                                         Nullable = true,
                                                         PresentationType = "string",
                                                         PropertyType = typeof(string).Name,
                                                         UsedInActionInput = false,
                                                         RequiredInActionInput = false,
                                                         UsedInActionOutput = true,
                                                         UsedInQuerySelect = false,
                                                     },
                                                     new PropertyDefinition
                                                     {
                                                         Name = "Resultmessage",
                                                         FullName = "Resultmessage",
                                                         Description = "Email Result Message",
                                                         IsPrimaryKey = false,
                                                         Nullable = true,
                                                         PresentationType = "string",
                                                         PropertyType = typeof(string).Name,
                                                         UsedInActionInput = false,
                                                         RequiredInActionInput = false,
                                                         UsedInActionOutput = true,
                                                         UsedInQuerySelect = false,
                                                     }													 
                                                     

                                                 }

                       }

                       //SMS Object Here?




                   };
        }

        public void ResetMetadata()
        {
        }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            return this.ActionDefinitions;
        }

        public IMethodDefinition RetrieveMethodDefinition(string objectName, bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IObjectDefinition RetrieveObjectDefinition(string objectName, bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return this.ObjectDefinitions.First(od => od.Name == objectName);
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return this.ObjectDefinitions;
        }

        public void Dispose()
        {
        }
    }
}