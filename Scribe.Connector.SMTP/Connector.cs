﻿using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Core.ConnectorApi.Common;
using Scribe.Core.ConnectorApi.ConnectionUI;
using Scribe.Core.ConnectorApi.Query;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using System.Data.Services.Client;

using System.IO;
using System.Net.Mail;
using Scribe.Core.ConnectorApi.Cryptography;
    

namespace Scribe.Connector.SMTP
{
    [ScribeConnector(
        ConnectorSettings.ConnectorTypeId,
        ConnectorSettings.Name,
        ConnectorSettings.Description,
        typeof(Connector),
        StandardConnectorSettings.SettingsUITypeName,
        StandardConnectorSettings.SettingsUIVersion,
        StandardConnectorSettings.ConnectionUITypeName,
        StandardConnectorSettings.ConnectionUIVersion,
        StandardConnectorSettings.XapFileName,
        new[] {  "Scribe.IS2.Target", "Scribe.IS." },
        ConnectorSettings.SupportsCloud, ConnectorSettings.ConnectorVersion)]
    public class Connector : IConnector
    {


        public bool IsConnected { get; set; }

        private string SMTPserver;
        private string SMTPport;
        private string username;
        private string emailAddress;
        private string EnableSSL;
        
        System.Net.NetworkCredential creds;

  
        public Guid ConnectorTypeId
        {
            get { return new Guid(ConnectorSettings.ConnectorTypeId); }
        }

        private static string KryptoKey
        {
            get { return "F06532DE-8049-41D8-9F62-4EB4F5896A80"; }
        }

        private MetadataProvider metadataProvider;
        public IMetadataProvider GetMetadataProvider()
        {
            this.metadataProvider = new MetadataProvider();
            return this.metadataProvider;
        }


        public string PreConnect(IDictionary<string, string> properties)
        {

            var entryDef =  new EntryDefinition
                                    {
                                        InputType = InputType.Text,
                                        IsRequired = true,
                                        Label = "Enable SSL",
                                        PropertyName = "enablessl"
                                    };
            entryDef.Options.Add("True", "True");
            entryDef.Options.Add("False", "False");


            var formDefinition = new FormDefinition
            {
                CompanyName = "Scribe Software",
                CryptoKey = KryptoKey,
                HelpUri = new Uri("http://www.scribesoft.com"),
                Entries =
                    new Collection<EntryDefinition>
                            {
                                new EntryDefinition
                                    {
                                        InputType = InputType.Text,
                                        IsRequired = true,
                                        Label = "SMTP Server",
                                        PropertyName = "SMTPServer"
                                    },
                                new EntryDefinition
                                    {
                                        InputType = InputType.Text,
                                        IsRequired = true,
                                        Label = "SMTP Port",
                                        PropertyName = "SMTPPort"
                                    },
                                new EntryDefinition
                                    {
                                        InputType = InputType.Text,
                                        IsRequired = true,
                                        Label = "Email Address",
                                        PropertyName = "FromEmailAddress"
                                    },
                                new EntryDefinition
                                    {
                                        InputType = InputType.Text,
                                        IsRequired = true,
                                        Label = "User Name",
                                        PropertyName = "UserName"
                                    },
                                new EntryDefinition
                                    {
                                        InputType = InputType.Password,
                                        IsRequired = true,
                                        Label = "Password",
                                        PropertyName = "Password"
                                    },
                                entryDef

                            }     
            };

            return formDefinition.Serialize();
        }

        public void Connect(IDictionary<string, string> properties)
        {

            this.username = properties["UserName"];
            this.emailAddress = properties["FromEmailAddress"];
            this.SMTPport = properties["SMTPPort"];
            this.SMTPserver = properties["SMTPServer"];
            this.EnableSSL = properties["enablessl"];

            string temppassword = Decryptor.Decrypt_AesManaged(properties["Password"], KryptoKey);

            creds = new System.Net.NetworkCredential(emailAddress, temppassword);

            
            this.IsConnected = true;
        }

        public void Disconnect()
        {
            

            this.IsConnected = false;
        }

        public MethodResult ExecuteMethod(MethodInput input)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            throw new NotImplementedException();
        }



        public OperationResult ExecuteOperation(OperationInput input)
        {
            var name = input.Name;

            // We are not implementing BULK
            DataEntity data = input.Input[0];

            // We do not require a lookup condition, but if you did, this is where you could get it.
            Expression lookup = input.LookupCondition[0];

            // Most Connectors will want to dispatch using the entity name, but we only have the one.
            var entityName = data.ObjectDefinitionFullName;
              
            var to = data.Properties["To"].ToString();
            var cc = data.Properties["CC"].ToString(); 

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.To.Add(to);
            //mail.CC.Add(cc);
            mail.From = new MailAddress(emailAddress, username, System.Text.Encoding.UTF8);
            mail.Subject = data.Properties["Subject"].ToString();
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = data.Properties["Body"].ToString();
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false ;

            //mail.Attachments = data.Properties["Attachment"].ToString();
            //mail.Priority = data.Properties["Priority"].ToString();
            
            mail.Priority = MailPriority.Normal;
            
            SmtpClient client = new SmtpClient();

            client.Credentials = creds;
            client.Port = System.Convert.ToInt16(SMTPport); 
            client.Host = SMTPserver;
            client.EnableSsl = System.Convert.ToBoolean(EnableSSL);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;


            string errorMessage = string.Empty; 
            try
                    {
                        client.Send(mail);
                    }
                    catch (Exception ex) 
                    {
                        Exception ex2 = ex;
                        while (ex2 != null)
                        {
                            errorMessage += ex2.ToString();
                            ex2 = ex2.InnerException;
                        }

                    } // end try 


                   data.Properties["SendResult"] = "";
                   data.Properties["Resultmessage"] = errorMessage;

                   client.Dispose();

            return new OperationResult
            {
                ErrorInfo = new ErrorResult[] { null },
                ObjectsAffected = new[] { 1 },
                Output = new[] { data },
                Success = new[] { true }
            };
            
        }





     }



}
